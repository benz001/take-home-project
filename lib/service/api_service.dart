import 'package:dio/dio.dart';
import 'package:dio_logger/dio_logger.dart';
import 'package:flutter/material.dart';
import 'package:take_home_project/helper/base_url.dart';
import 'package:take_home_project/helper/access_token_helper.dart';
import 'package:take_home_project/model/create_faq_model.dart';
import 'package:take_home_project/model/delete_faq_model.dart';
import 'package:take_home_project/model/detail_faq_model.dart';
import 'package:take_home_project/model/faq_model.dart';
import 'package:take_home_project/model/login_model.dart';
import 'package:take_home_project/model/logout_model.dart';
import 'package:take_home_project/model/update_faq_model.dart';

class APIService {
  Dio dio = Dio()..interceptors.add(dioLoggerInterceptor);
  final AccessTokenHelper _accessTokenHelper = AccessTokenHelper();

  Future<LoginModel> postLogin(String nip, String password) async {
    try {
      final response = await dio.post('${BaseURL.mainURL}/api/v1/auth/login',
          data: {"nip": nip, "password": password});
      debugPrint('success from postLogin');
      return LoginModel.fromJson(response.data);
    } on DioError catch (e) {
      debugPrint('error from postLogin: ${e.response?.data}');
      return LoginModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data["message"].toString() ?? "",
          data: null);
    }
  }

  Future<LogoutModel> postLogout(String nip, String password) async {
    final token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.post('${BaseURL.mainURL}/api/v1/auth/logout',
          options: Options(headers: {"Authorization": "bearer $token"}));
      return LogoutModel.fromJson(response.data);
    } on DioError catch (e) {
      return LogoutModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data["message"].toString() ?? "",
          data: null);
    }
  }

  Future<FaqModel> getListFaq() async {
    final token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.get('${BaseURL.mainURL}/api/v1/superadmin/faq',
          options: Options(headers: {"Authorization": "bearer $token"}));
      return FaqModel.fromJson(response.data);
    } on DioError catch (e) {
      return FaqModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data["message"].toString() ?? "",
          data: [],
          pagination: Pagination());
    }
  }

   Future<FaqModel> getListFaqLoadMore(int page) async {
    final token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.get('${BaseURL.mainURL}/api/v1/superadmin/faq?page=$page',
          options: Options(headers: {"Authorization": "bearer $token"}));
      return FaqModel.fromJson(response.data);
    } on DioError catch (e) {
      return FaqModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data["message"].toString() ?? "",
          data: [],
          pagination: Pagination());
    }
  }

  Future<DetailFaqModel> getDetailFaq(int faqId) async {
    final token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.get('${BaseURL.mainURL}/api/v1/superadmin/faq/$faqId',
          options: Options(headers: {"Authorization": "bearer $token"}));
      return DetailFaqModel.fromJson(response.data);
    } on DioError catch (e) {
      return DetailFaqModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data["message"].toString() ?? "",
          data: null,
        );
    }
  }

  Future<CreateFaqModel> postCreateFaq(
      String pertanyaan, String jawaban, bool statusPublish) async {
    final String? token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.post(
          '${BaseURL.mainURL}/api/v1/superadmin/faq',
          options: Options(headers: {"Authorization": "bearer $token"}),
          data: {
            "pertanyaan": pertanyaan,
            "jawaban": jawaban,
            "status_publish": statusPublish
          });
      return CreateFaqModel.fromJson(response.data);
    } on DioError catch (e) {
      return CreateFaqModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data['message'],
          data: null);
    }
  }

  Future<UpdateFaqModel> postUpdateFaq(
      int faqId, String pertanyaan, String jawaban, bool statusPublish) async {
    final String? token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.post(
          '${BaseURL.mainURL}/api/v1/superadmin/faq/$faqId',
          options: Options(headers: {"Authorization": "bearer $token"}),
           data: {
            "pertanyaan": pertanyaan,
            "jawaban": jawaban,
            "status_publish": statusPublish
          });
      return UpdateFaqModel.fromJson(response.data);
    } on DioError catch (e) {
      return UpdateFaqModel(
          code: e.response?.statusCode ?? 0,
          message:e.response?.data["message"].toString() ?? "",
          data: null);
    }
  }

  Future<DeleteFaqModel> postDeleteFaq(int faqId) async {
    final String? token = await _accessTokenHelper.getAccessToken();
    try {
      final response = await dio.delete(
          '${BaseURL.mainURL}/api/v1/superadmin/faq/$faqId',
          options: Options(headers: {"Authorization": "bearer $token"}));
      return DeleteFaqModel.fromJson(response.data);
    } on DioError catch (e) {
      return DeleteFaqModel(
          code: e.response?.statusCode ?? 0,
          message: e.response?.data["message"].toString() ?? "",
          data: null);
    }
  }
}
