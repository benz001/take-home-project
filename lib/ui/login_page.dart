import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/authentication_controller.dart';
import 'package:take_home_project/reusable_widget/primary_button.dart';
import 'package:take_home_project/reusable_widget/primary_button_loading.dart';
import 'package:take_home_project/reusable_widget/primary_text_field.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';

class LoginPage extends StatelessWidget {
  static const routeName = '/login-page';
  LoginPage({super.key});
  final AuthenticationController _authhenticationController =
      Get.find<AuthenticationController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        color: Colors.white,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            _buildLabelLogin(),
            const SizedBox(
              height: 20,
            ),
            _buildFormLogin(),
            const SizedBox(
              height: 20,
            ),
            _buildCheckboxShowPassword(),
            const SizedBox(
              height: 20,
            ),
            _buildLoginButton(),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildLabelLogin() {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: const Text(
        'Selamat Data di Aplikasi FAQ :)',
        textAlign: TextAlign.center,
        style: TextStyle(fontSize: 16, fontWeight: FontWeight.w700),
      ),
    );
  }

  Widget _buildFormLogin() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: FormBuilder(
        key: _authhenticationController.formKey,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        child: Container(
          color: Colors.transparent,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                color: Colors.transparent,
                child: PrimaryTextField(
                  name: 'email',
                  controller:
                      _authhenticationController.emaillTextEditingController,
                  validator: FormBuilderValidators.compose([
                    FormBuilderValidators.required(),
                  ]),
                  hintText: 'Email/NIP',
                  keyboardType: TextInputType.emailAddress,
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Obx(() => Container(
                    width: double.infinity,
                    color: Colors.transparent,
                    child: PrimaryTextField(
                      name: 'password',
                      controller: _authhenticationController
                          .passwordTextEditingController,
                      validator: FormBuilderValidators.compose([
                        FormBuilderValidators.required(),
                      ]),
                      hintText: 'Password',
                      obsecureText:
                          _authhenticationController.isShowPassword.value
                              ? false
                              : true,
                    ),
                  )),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLoginButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Obx(() => Container(
          width: double.infinity,
          height: 45,
          color: Colors.transparent,
          child: !_authhenticationController.isLoadingLogin.value
              ? PrimaryButton(
                  onPressed: () {
                    if (_authhenticationController.formKey.currentState!
                        .saveAndValidate()) {
                      _authhenticationController.loginProcess();
                    }
                  },
                  text: 'LOGIN',
                )
              : const PrimaryButtonLoading())),
    );
  }

  Widget _buildCheckboxShowPassword() {
    return Container(
      width: double.infinity,
      color: Colors.transparent,
      child: Row(
        children: [
          Obx(() => Checkbox(
              value: _authhenticationController.isShowPassword.value,
              onChanged: (v) {
                _authhenticationController.showPassword(v!);
              })),
          Expanded(
              child: Container(
                  color: Colors.transparent,
                  child: const Text('Show password')))
        ],
      ),
    );
  }
}
