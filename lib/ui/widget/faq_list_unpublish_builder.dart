import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/faq_list_controller.dart';
import 'package:take_home_project/model/faq_model.dart';
import 'package:take_home_project/reusable_widget/primary_button.dart';
import 'package:take_home_project/ui/widget/bottom_sheet_unpublish_faq.dart';

class FaqListUnPublishBuilder extends StatelessWidget {
  final FaqListController controller;
  const FaqListUnPublishBuilder({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.grey[300],
      child: Builder(builder: (context) {
         if (controller.isLoadingUnPublishFaqList) {
          return const Center(child: CircularProgressIndicator());
        } else {
          if (controller.isSuccessUnPublishFaqList) {
            return controller.listUnPublishFaq.isNotEmpty
                ? _buildListFaqUnPublish(context, controller.listUnPublishFaq)
                : const Center(
                    child: Text('FAQ Masih kosong'),
                  );
          } else {
            return Center(
              child: PrimaryButton(
                onPressed: () {
                  controller.getUnPublishFaqListProcess();
                },
                text: 'Reload',
              ),
            );
          }
        }
      }),
    );
  }

  Widget _buildListFaqUnPublish(BuildContext context, List<DataFaq> listFaq) {
    return RefreshIndicator(
       onRefresh: () async {
        controller.getUnPublishFaqListProcess();
      },
      child: ListView.builder(
        itemCount: listFaq.length+1,
        controller: controller.scrollControllerFaqListUnPublish,
        itemBuilder: (context, index) {
            if (index == listFaq.length) {
            return Obx(() {
              if (controller.isLoadingMoreUnPublishFaqList.value) {
                return Container(
                  width: double.infinity,
                  height: 45,
                  color: Colors.white,
                  margin: const EdgeInsets.only(top: 10),
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else {
                return Container();
                    
              }
            });
          }
          return _buildItemFaqUnPublish(context, index, listFaq[index]);
        },
      ),
    );
  }

  Widget _buildItemFaqUnPublish(BuildContext context,int index,DataFaq itemFaq) {
    return Container(
      padding: const EdgeInsets.all(10),
      color: Colors.white,
      margin:  const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [const Text('Pertanyaan',style: TextStyle(fontWeight: FontWeight.bold)), Text(itemFaq.pertanyaan ?? '')],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            width: double.infinity,
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
               const Text('Jawaban:',style: TextStyle(fontWeight: FontWeight.bold)),
                Text(
                  itemFaq.jawaban ?? '-',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          const SizedBox(height: 5,),
          Container(
            width: double.infinity,
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                     controller.faqId.value = itemFaq.id!;
                    controller.questionPublishFaqController.text =
                        itemFaq.pertanyaan ?? '';
                    controller.answerPublishFaqController.text =
                        itemFaq.jawaban ?? '';
                    controller.valueDropdownPublishFaq.value =
                        itemFaq.statusPublish == 1 ? 'publish' : 'unpublish';
                    BottomSheetUnPublishFAQ(controller: controller)
                        .publishedFaq(context);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.lightBlue,
                    child: const Icon(
                      Icons.publish,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    controller.faqId.value = itemFaq.id!;
                    controller.questionPublishFaqController.text =
                        itemFaq.pertanyaan ?? '';
                    controller.answerPublishFaqController.text =
                        itemFaq.jawaban ?? '';
                    controller.valueDropdownPublishFaq.value =
                        itemFaq.statusPublish == 1 ? 'publish' : 'unpublish';
                    Future.delayed(
                        Duration.zero,
                        () => BottomSheetUnPublishFAQ(controller: controller)
                            .updateUnPublishFaq(context));
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.green,
                    child: const Icon(
                      Icons.edit,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    controller.faqId.value = itemFaq.id!;
                    controller.detailFaqProccess().then((value) {
                      if (value) {
                        Future.delayed(
                            Duration.zero,
                            () => BottomSheetUnPublishFAQ(controller: controller)
                                .detailUnPublishFaq(context));
                      }
                    });
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.yellow[600],
                    child: const Icon(
                      Icons.remove_red_eye,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                     controller.faqId.value = itemFaq.id!;
                    Future.delayed(
                        Duration.zero,
                        () => BottomSheetUnPublishFAQ(controller: controller)
                            .deleteUnPublishFaq(context));
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.red,
                    child: const Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
