import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/faq_list_controller.dart';
import 'package:take_home_project/helper/remove_string.dart';
import 'package:take_home_project/reusable_widget/customize_button.dart';
import 'package:take_home_project/reusable_widget/primary_button.dart';
import 'package:take_home_project/reusable_widget/primary_text_field.dart';

class BottomSheetUnPublishFAQ extends StatelessWidget {
  final FaqListController controller;
  const BottomSheetUnPublishFAQ({super.key, required this.controller});

  createUnPublishFaq(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.60,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: FormBuilder(
                    key: controller.formKeyCreateUnPublishFaq,
                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Container(
                        color: Colors.transparent,
                        margin: const EdgeInsets.only(top: 10),
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  height: 25,
                                  width: 25,
                                  color: Colors.grey[300],
                                  child: Icon(Icons.close),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              color: Colors.transparent,
                              child: PrimaryTextField(
                                name: 'question',
                                controller:
                                    controller.questionUnPublishFaqController,
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ]),
                                hintText: 'Pertanyaan',
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              color: Colors.transparent,
                              child: PrimaryTextField(
                                name: 'answer',
                                controller:
                                    controller.answerUnPublishFaqController,
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ]),
                                hintText: 'Jawaban',
                                maxLines: 4,
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.grey)),
                              child: Obx(() => Container(
                                    color: Colors.white,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        // Initial Value
                                        value: controller
                                            .valueDropdownUnPublishFaq.value,
                                        // Down Arrow Icon
                                        icon: const Icon(
                                            Icons.keyboard_arrow_down),
                                        // Array list of items
                                        items: controller
                                            .listDropdownUnPublishFaq
                                            .map((String items) {
                                          return DropdownMenuItem(
                                            value: items,
                                            child: Text(items.capitalize!),
                                          );
                                        }).toList(),
                                        // After selecting the desired option,it will
                                        // change button value to selected value
                                        onChanged: (value) {
                                          controller.valueDropdownUnPublishFaq
                                              .value = value!;
                                        },
                                      ),
                                    ),
                                  )),
                            ),
                          ],
                        )),
                  )),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      width: double.infinity,
                      height: 45,
                      margin: const EdgeInsets.only(bottom: 10),
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          child: PrimaryButton(
                            onPressed: () {
                              if (controller
                                  .formKeyCreateUnPublishFaq.currentState!
                                  .saveAndValidate()) {
                                controller.createFaqUnPublishProccess().then((value) {
                                  if (value) {
                                    Get.back();
                                    controller.getUnPublishFaqListProcess();
                                  }
                                });
                              }
                            },
                            text: 'Tambah',
                          ))))
            ],
          ),
        ));
  }

  updateUnPublishFaq(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.60,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: FormBuilder(
                    key: controller.formKeyUpdateUnPublishFaq,
                    // autovalidateMode: AutovalidateMode.onUserInteraction,
                    child: Container(
                        color: Colors.transparent,
                        margin: const EdgeInsets.only(top: 10),
                        child: Column(
                          children: [
                            GestureDetector(
                              onTap: () {
                                Get.back();
                              },
                              child: Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                  height: 25,
                                  width: 25,
                                  color: Colors.grey[300],
                                  child: Icon(Icons.close),
                                ),
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              color: Colors.transparent,
                              child: PrimaryTextField(
                                  name: 'question',
                                  controller:
                                      controller.questionUnPublishFaqController,
                                  validator: FormBuilderValidators.compose([
                                    FormBuilderValidators.required(),
                                  ]),
                                  hintText: 'Pertanyaan'),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              color: Colors.transparent,
                              child: PrimaryTextField(
                                name: 'answer',
                                controller:
                                    controller.answerUnPublishFaqController,
                                validator: FormBuilderValidators.compose([
                                  FormBuilderValidators.required(),
                                ]),
                                hintText: 'Jawaban',
                                maxLines: 4,
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(color: Colors.grey)),
                              child: Obx(() => Container(
                                    color: Colors.white,
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 5),
                                    child: DropdownButtonHideUnderline(
                                      child: DropdownButton(
                                        isExpanded: true,
                                        // Initial Value
                                        value: controller
                                            .valueDropdownUnPublishFaq.value,
                                        // Down Arrow Icon
                                        icon: const Icon(
                                            Icons.keyboard_arrow_down),
                                        // Array list of items
                                        items: controller
                                            .listDropdownUnPublishFaq
                                            .map((String items) {
                                          return DropdownMenuItem(
                                            value: items,
                                            child: Text(items.capitalize!),
                                          );
                                        }).toList(),
                                        // After selecting the desired option,it will
                                        // change button value to selected value
                                        onChanged: (value) {
                                          controller.valueDropdownUnPublishFaq
                                              .value = value!;
                                        },
                                      ),
                                    ),
                                  )),
                            ),
                          ],
                        )),
                  )),
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                      width: double.infinity,
                      height: 45,
                      margin: const EdgeInsets.only(bottom: 10),
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          child: PrimaryButton(
                            onPressed: () {
                              if (controller
                                  .formKeyUpdateUnPublishFaq.currentState!
                                  .saveAndValidate()) {
                                controller.updateFaqUnPublishProccess().then((value) {
                                  if (value) {
                                    Get.back();
                                    controller.getUnPublishFaqListProcess();
                                  }
                                });
                              }
                            },
                            text: 'Update',
                          ))))
            ],
          ),
        ));
  }

  deleteUnPublishFaq(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Get.back();
                                },
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 25,
                                    width: 25,
                                    color: Colors.grey[300],
                                    child: Icon(Icons.close),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: double.infinity,
                                color: Colors.transparent,
                                child: const Text(
                                    'Apa Anda yakin ingin menghapus FAQ ?'),
                              ),
                            ],
                          )))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: CustomizeButton(
                                  backgroundColor: Colors.red,
                                  onPressed: () {
                                    controller
                                        .deleteFaqPublishProccess()
                                        .then((value) {
                                      if (value) {
                                        Get.back();
                                        controller.getUnPublishFaqListProcess();
                                      }
                                    });
                                  },
                                  text: 'Yes',
                                ))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: PrimaryButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  text: 'No',
                                ))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  publishedFaq(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child: Column(
                            children: [
                              GestureDetector(
                                onTap: () {
                                  Get.back();
                                },
                                child: Align(
                                  alignment: Alignment.topRight,
                                  child: Container(
                                    height: 25,
                                    width: 25,
                                    color: Colors.grey[300],
                                    child: Icon(Icons.close),
                                  ),
                                ),
                              ),
                              const SizedBox(
                                height: 5,
                              ),
                              Container(
                                width: double.infinity,
                                color: Colors.transparent,
                                child: const Text(
                                    'Apa Anda yakin ingin menmpublish FAQ ?'),
                              ),
                            ],
                          )))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: CustomizeButton(
                                  backgroundColor: Colors.red,
                                  onPressed: () {
                                    controller
                                        .publishFaqProccess()
                                        .then((value) {
                                      if (value) {
                                        Get.back();
                                        Future.delayed(
                                            Duration.zero,
                                            () => controller
                                                .getUnPublishFaqListProcess());
                                      }
                                    });
                                  },
                                  text: 'Yes',
                                ))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: PrimaryButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  text: 'No',
                                ))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  detailUnPublishFaq(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.70,
            padding: const EdgeInsets.all(10),
            decoration: const BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(10),
                    topRight: Radius.circular(10))),
            child: Obx(() {
              if (controller.isLoadingDetailFaq.value) {
                return const Center(child: CircularProgressIndicator());
              } else {
                if (controller.isSuccessDetailFaq.value) {
                  return Column(
                    children: [
                      GestureDetector(
                        onTap: () {
                          Get.back();
                        },
                        child: Align(
                          alignment: Alignment.topRight,
                          child: Container(
                            height: 25,
                            width: 25,
                            color: Colors.grey[300],
                            child: Icon(Icons.close),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Container(
                          width: double.infinity,
                          color: Colors.transparent,
                          child: const Text('Detail FAQ',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  decoration: TextDecoration.underline))),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                          width: double.infinity,
                          color: Colors.transparent,
                          child: Text(
                            RemoveString.afterChara(
                                controller.dataDetailFaq.updatedAt
                                        ?.toIso8601String() ??
                                    '-',
                                'T'),
                            textAlign: TextAlign.right,
                          )),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        color: Colors.transparent,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Pertanyaan',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            Text(controller.dataDetailFaq.pertanyaan ?? '')
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        color: Colors.transparent,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Text('Jawaban:',
                                style: TextStyle(fontWeight: FontWeight.bold)),
                            Text(
                              controller.dataDetailFaq.jawaban ?? '-',
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                } else {
                  return Center(
                    child: PrimaryButton(text: 'Reload', onPressed: () {}),
                  );
                }
              }
            })));
  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
