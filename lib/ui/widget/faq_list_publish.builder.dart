import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/faq_list_controller.dart';
import 'package:take_home_project/model/faq_model.dart';
import 'package:take_home_project/reusable_widget/primary_button.dart';
import 'package:take_home_project/reusable_widget/primary_text_field.dart';
import 'package:take_home_project/ui/widget/bottom_sheet_publish_faq.dart';

class FaqListPublishBuilder extends StatelessWidget {
  final FaqListController controller;
  const FaqListPublishBuilder({super.key, required this.controller});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.grey[300],
      child: Builder(builder: (context) {
        if (controller.isLoadingPublishFaqList) {
          return const Center(child: CircularProgressIndicator());
        } else {
          if (controller.isSuccessPublishFaqList) {
            return controller.listPublishFaq.isNotEmpty
                ? _buildListFaqPublish(context, controller.listPublishFaq)
                : const Center(
                    child: Text('FAQ Masih kosong'),
                  );
          } else {
            return Center(
              child: PrimaryButton(
                onPressed: () {
                  controller.getPublishFaqListProcess();
                },
                text: 'Reload',
              ),
            );
          }
        }
      }),
    );
  }

  Widget _buildListFaqPublish(BuildContext context, List<DataFaq> listFaq) {
    return RefreshIndicator(
      onRefresh: ()async{
        controller.getPublishFaqListProcess();
      },
      child: ListView.builder(
        controller: controller.scrollControllerFaqListPublish,
        itemCount: listFaq.length + 1,
        itemBuilder: (context, index) {
          if (index == listFaq.length) {
            return Obx(() {
              debugPrint(
                  'isLoadingMorePublishFaqList: ${controller.isLoadingMorePublishFaqList}');
              if (controller.isLoadingMorePublishFaqList.value) {
                return Container(
                  width: double.infinity,
                  height: 45,
                  color: Colors.white,
                  margin: const EdgeInsets.only(top: 10),
                  child: const Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              } else {
                return Container();
              }
            });
          }
          return _buildItemFaqPublish(context, index, listFaq[index]);
        },
      ),
    );
  }

  Widget _buildItemFaqPublish(BuildContext context, index, DataFaq itemFaq) {
    return Container(
      padding: const EdgeInsets.all(10),
      color: Colors.white,
      margin: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Container(
            width: double.infinity,
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Pertanyaan',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text(itemFaq.pertanyaan ?? '')
              ],
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Container(
            width: double.infinity,
            color: Colors.transparent,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text('Jawaban:',
                    style: TextStyle(fontWeight: FontWeight.bold)),
                Text(
                  itemFaq.jawaban ?? '-',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
              ],
            ),
          ),
          const SizedBox(
            height: 5,
          ),
          Container(
            width: double.infinity,
            color: Colors.transparent,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                GestureDetector(
                  onTap: () {
                    controller.faqId.value = itemFaq.id!;
                    controller.questionPublishFaqController.text =
                        itemFaq.pertanyaan ?? '';
                    controller.answerPublishFaqController.text =
                        itemFaq.jawaban ?? '';
                    controller.valueDropdownPublishFaq.value =
                        itemFaq.statusPublish == 1 ? 'publish' : 'unpublish';
                    BottomSheetPublishFAQ(controller: controller)
                        .unPublishedFaq(context);
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.lightBlue,
                    child: const Icon(
                      Icons.unpublished,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    controller.faqId.value = itemFaq.id!;
                    controller.questionPublishFaqController.text =
                        itemFaq.pertanyaan ?? '';
                    controller.answerPublishFaqController.text =
                        itemFaq.jawaban ?? '';
                    controller.valueDropdownPublishFaq.value =
                        itemFaq.statusPublish == 1 ? 'publish' : 'unpublish';
                    Future.delayed(
                        Duration.zero,
                        () => BottomSheetPublishFAQ(controller: controller)
                            .updatePublishFaq(context));
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.green,
                    child: const Icon(
                      Icons.edit,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    controller.faqId.value = itemFaq.id!;
                    controller.detailFaqProccess().then((value) {
                      if (value) {
                        Future.delayed(
                            Duration.zero,
                            () => BottomSheetPublishFAQ(controller: controller)
                                .detailPublishFaq(context));
                      }
                    });
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.yellow[600],
                    child: const Icon(
                      Icons.remove_red_eye,
                      color: Colors.white,
                    ),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    controller.faqId.value = itemFaq.id!;
                    Future.delayed(
                        Duration.zero,
                        () => BottomSheetPublishFAQ(controller: controller)
                            .deletePublishFaq(context));
                  },
                  child: Container(
                    width: 30,
                    height: 30,
                    color: Colors.red,
                    child: const Icon(
                      Icons.delete,
                      color: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
