import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/faq_list_controller.dart';
import 'package:take_home_project/helper/access_token_helper.dart';
import 'package:take_home_project/reusable_widget/customize_button.dart';
import 'package:take_home_project/reusable_widget/primary_button.dart';
import 'package:take_home_project/reusable_widget/primary_text_field.dart';
import 'package:take_home_project/ui/login_page.dart';

class BottomSheetLogout extends StatelessWidget {
  const BottomSheetLogout({super.key});

  

  logoutApp(BuildContext context) {
    Get.bottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        Container(
          width: double.infinity,
          height: MediaQuery.of(context).size.height * 0.25,
          decoration: const BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(10), topRight: Radius.circular(10))),
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Stack(
            children: [
              Align(
                  alignment: Alignment.topCenter,
                  child: Container(
                      color: Colors.transparent,
                      child: Container(
                          color: Colors.transparent,
                          margin: const EdgeInsets.only(top: 10),
                          child: const Text(
                              'Apa Anda yakin ingin keluar dari Apps ?')))),
              Align(
                  alignment: Alignment.center,
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: CustomizeButton(
                                  backgroundColor: Colors.red,
                                  onPressed: () {
                                    AccessTokenHelper().setAccessToken('');
                Get.offAndToNamed(LoginPage.routeName);
                                  },
                                  text: 'Yes',
                                ))),
                      ),
                      const SizedBox(
                        width: 15,
                      ),
                      Expanded(
                        child: Container(
                            width: double.infinity,
                            height: 45,
                            color: Colors.transparent,
                            child: Container(
                                color: Colors.transparent,
                                child: PrimaryButton(
                                  onPressed: () {
                                    Get.back();
                                  },
                                  text: 'No',
                                ))),
                      ),
                    ],
                  ))
            ],
          ),
        ));
  }

  

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
