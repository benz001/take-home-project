import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/faq_list_controller.dart';
import 'package:take_home_project/helper/access_token_helper.dart';
import 'package:take_home_project/ui/login_page.dart';
import 'package:take_home_project/ui/splash_page.dart';
import 'package:take_home_project/ui/widget/bottom_sheet_logout.dart';
import 'package:take_home_project/ui/widget/bottom_sheet_unpublish_faq.dart';
import 'package:take_home_project/ui/widget/faq_list_publish.builder.dart';
import 'package:take_home_project/ui/widget/faq_list_unpublish_builder.dart';

import 'widget/bottom_sheet_publish_faq.dart';

class FaqListPage extends StatelessWidget {
  static const routeName = '/faq-list-page';
  FaqListPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<FaqListController>(builder: (controller) {
      return Scaffold(
        appBar: AppBar(
          title: const Text('List FAQ'),
          automaticallyImplyLeading: false,
          centerTitle: true,
          actions: [
            PopupMenuButton(
                // add icon, by default "3 dot" icon
                // icon: Icon(Icons.book)
                itemBuilder: (context) {
              return [
               const PopupMenuItem<int>(
                  value: 0,
                  child: Text("Logout"),
                ),
              ];
            }, onSelected: (value) {
              if (value == 0) {
                const BottomSheetLogout().logoutApp(context);
              }
            }),
          ],
          bottom: TabBar(
              onTap: (value) {
                if (value == 0) {
                  controller.getPublishFaqListProcess();
                  controller.publishStatusCurrently = PublishStatus.PUBLISH;
                } else {
                  controller.getUnPublishFaqListProcess();
                  controller.publishStatusCurrently = PublishStatus.UNPUBLISH;
                }
              },
              
              controller: controller.tabController,
              tabs: controller.listTabs),
        ),
        body: TabBarView(controller: controller.tabController, children: [
          FaqListPublishBuilder(
            controller: controller,
          ),
          FaqListUnPublishBuilder(
            controller: controller,
          )
        ]),
         floatingActionButtonLocation: FloatingActionButtonLocation.startDocked,
        floatingActionButton: FloatingActionButton(
          
          onPressed: () {
            if (controller.publishStatusCurrently == PublishStatus.PUBLISH) {
              controller.clearTexEditPublishFaq();
              BottomSheetPublishFAQ(
                controller: controller,
              ).createPublishFaq(context);
            } else {
              controller.clearTexEditUnPublishFaq();
              BottomSheetUnPublishFAQ(
                controller: controller,
              ).createUnPublishFaq(context);
            }
          },
          child: Icon(Icons.add),
        ),
      );
    });
  }
}
