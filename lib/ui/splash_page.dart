import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_project/controller/splash_controller.dart';

class SplashPage extends StatelessWidget {
  static const routeName = '/splash-page';
  SplashPage({super.key});
  final SplashController _splashController = Get.find<SplashController>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      width: double.infinity,
      height: double.infinity,
      color: Colors.white,
      alignment: Alignment.center,
      child: GetBuilder<SplashController>(
          init: _splashController,
          builder: (controller) {
            return const Text(
              'Selamat Datang',
              style: TextStyle(fontWeight: FontWeight.w700, fontSize: 16),
            );
          }),
    ));
  }
}
