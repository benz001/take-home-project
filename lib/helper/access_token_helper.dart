import 'package:shared_preferences/shared_preferences.dart';

class AccessTokenHelper {
  void setAccessToken(String token) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('access_token', token);
  }

   Future<String?> getAccessToken()async{
    final prefs = await SharedPreferences.getInstance();
    final accessToken = prefs.getString('access_token');
    return accessToken;
  }
}
