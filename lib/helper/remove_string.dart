class RemoveString {
  static String beforeChara(String param, String chara) {
//Removes everything after first '.'
    try {
      String result = param.substring(param.indexOf(chara) + 1);
      print(result);
      return result;
    } catch (e) {
      return param;
    }
  }

  static String afterChara(String param, String chara) {
//Removes everything after first '.'
    try {
      String result = param.substring(0, param.indexOf(chara));
      print(result);
      return result;
    } catch (e) {
      return param;
    }
  }
}
