import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:take_home_project/model/detail_faq_model.dart';
import 'package:take_home_project/model/faq_model.dart';
import 'package:take_home_project/service/api_service.dart';

class FaqListController extends GetxController
    with GetSingleTickerProviderStateMixin {
  late TabController tabController;
  final List<Widget> listTabs = [
    const Tab(text: 'Publish'),
    const Tab(text: 'Unpublish')
  ];

  final APIService _apiService = APIService();

  bool isLoadingPublishFaqList = false;
  bool isSuccessPublishFaqList = false;
  RxBool isLoadingMorePublishFaqList = false.obs;
  RxBool isSuccessMorePublishFaqList = false.obs;
  int nextPageMorePublishFaqList = 1;
  List<DataFaq> listPublishFaq = [];

  bool isLoadingUnPublishFaqList = false;
  bool isSuccessUnPublishFaqList = false;
  RxBool isLoadingMoreUnPublishFaqList = false.obs;
  RxBool isSuccessMoreUnPublishFaqList = false.obs;
  int nextPageMoreUnPublishFaqList = 1;
  List<DataFaq> listUnPublishFaq = [];
  DataDetailFaq dataDetailFaq = DataDetailFaq();

  RxString valueDropdownPublishFaq = 'publish'.obs;
  List<String> listDropdownPublishFaq = ['publish', 'unpublish'];

  RxString valueDropdownUnPublishFaq = 'unpublish'.obs;
  List<String> listDropdownUnPublishFaq = ['publish', 'unpublish'];

  TextEditingController questionPublishFaqController =
      TextEditingController(text: '');
  RxBool isHidequestionPublishFaqController =
     false.obs;
  TextEditingController answerPublishFaqController =
      TextEditingController(text: '');

  TextEditingController questionUnPublishFaqController =
      TextEditingController(text: '');
  TextEditingController answerUnPublishFaqController =
      TextEditingController(text: '');

  PublishStatus publishStatusCurrently = PublishStatus.PUBLISH;

  RxBool isLoadingCreateFaq = false.obs;
  RxBool isSuccessCreateFaq = false.obs;

  RxBool isLoadingUpdateFaq = false.obs;
  RxBool isSuccessUpdateFaq = false.obs;

  RxBool isLoadingDeleteFaq = false.obs;
  RxBool isSuccessDeleteFaq = false.obs;

  RxBool isLoadingDetailFaq = false.obs;
  RxBool isSuccessDetailFaq = false.obs;

  RxInt faqId = 0.obs;

  late ScrollController scrollControllerFaqListPublish;
  late ScrollController scrollControllerFaqListUnPublish;

  final formKeyCreatePublishFaq = GlobalKey<FormBuilderState>();
  final formKeyCreateUnPublishFaq = GlobalKey<FormBuilderState>();

  final formKeyUpdatePublishFaq = GlobalKey<FormBuilderState>();
  final formKeyUpdateUnPublishFaq = GlobalKey<FormBuilderState>();



  @override
  void onInit() {
    tabController = TabController(length: 2, vsync: this);
    scrollControllerFaqListPublish = ScrollController();
    scrollControllerFaqListUnPublish = ScrollController();
    super.onInit();
  }

  @override
  void onReady() {
    getPublishFaqListProcess();
    Future.delayed(Duration.zero,()=>getUnPublishFaqListProcess());
    tabController.addListener(() {
      if (tabController.index == 0) {
        getPublishFaqListProcess();
      }else{
        getUnPublishFaqListProcess();
      }
      
    });
    scrollControllerFaqListPublish.addListener(() {
      if (scrollControllerFaqListPublish.position.pixels ==
          scrollControllerFaqListPublish.position.maxScrollExtent) {
        getPublishFaqListProcessLoadMore();
      }
    });

    scrollControllerFaqListUnPublish.addListener(() {
      if (scrollControllerFaqListUnPublish.position.pixels ==
          scrollControllerFaqListUnPublish.position.maxScrollExtent) {
        getUnPublishFaqListProcessLoadMore();
      }
    });
    super.onReady();
  }

  void getPublishFaqListProcess() async {
    clearlistPublishFaq();
    isLoadingPublishFaqList = true;
    isSuccessPublishFaqList = false;
    update();
    final result = await _apiService.getListFaq();
    if (result.code == 200) {
      isLoadingPublishFaqList = false;
      isSuccessPublishFaqList = true;
      listPublishFaq = result.data
              ?.where((element) => element.statusPublish == 1)
              .toList() ??
          [];
      update();
    } else {
      isLoadingPublishFaqList = false;
      isSuccessPublishFaqList = false;
      update();
    }
  }

  void getPublishFaqListProcessLoadMore() async {
    debugPrint('getPublishFaqListProcessLoadMore');
    isLoadingMorePublishFaqList.value = true;
    isSuccessMorePublishFaqList.value = false;
    final result =
        await _apiService.getListFaqLoadMore(nextPageMorePublishFaqList++);
    if (result.code == 200) {
      isLoadingMorePublishFaqList.value = false;
      isSuccessMorePublishFaqList.value = true;
      listPublishFaq.addAll(result.data
              ?.where((element) => element.statusPublish == 1)
              .toList() ??
          []);
    } else {
      isLoadingMorePublishFaqList.value = false;
      isSuccessMorePublishFaqList.value = false;
    }
  }

  void getUnPublishFaqListProcess() async {
    clearlistUnPublishFaq();
    isLoadingUnPublishFaqList = true;
    isSuccessUnPublishFaqList = false;
    update();
    final result = await _apiService.getListFaq();
    if (result.code == 200) {
      isLoadingUnPublishFaqList = false;
      isSuccessUnPublishFaqList = true;
      listUnPublishFaq = result.data
              ?.where((element) => element.statusPublish == 0)
              .toList() ??
          [];
      update();
    } else {
      isLoadingUnPublishFaqList = false;
      isSuccessUnPublishFaqList = false;
      update();
    }
  }

  void getUnPublishFaqListProcessLoadMore() async {
    debugPrint('getUnPublishFaqListProcessLoadMore');
    isLoadingMoreUnPublishFaqList.value = true;
    isSuccessMoreUnPublishFaqList.value = false;
    update();
    final result =
        await _apiService.getListFaqLoadMore(nextPageMoreUnPublishFaqList++);
    if (result.code == 200) {
      isLoadingMoreUnPublishFaqList.value = false;
      isSuccessMoreUnPublishFaqList.value = true;
      listUnPublishFaq.addAll(result.data
              ?.where((element) => element.statusPublish == 0)
              .toList() ??
          []);

      update();
    } else {
      isLoadingMoreUnPublishFaqList.value = false;
      isSuccessMoreUnPublishFaqList.value = false;
      update();
    }
  }

  void clearlistPublishFaq() {
    listPublishFaq.clear();
    update();
  }

  void clearlistUnPublishFaq() {
    listUnPublishFaq.clear();
    update();
  }

  void clearTexEditPublishFaq() {
    questionPublishFaqController.clear();
    answerPublishFaqController.clear();
  }

  void clearTexEditUnPublishFaq() {
    questionUnPublishFaqController.clear();
    answerUnPublishFaqController.clear();
  }

  Future<bool> createFaqPublishProccess() async {
    isLoadingCreateFaq.value = true;
    isSuccessCreateFaq.value = false;
    final result = await _apiService.postCreateFaq(
        questionPublishFaqController.text,
        answerPublishFaqController.text,
        valueDropdownPublishFaq.value == 'publish' ? true : false);
    if (result.code == 200) {
      isLoadingCreateFaq.value = false;
      isSuccessCreateFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingCreateFaq.value = false;
      isSuccessCreateFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> createFaqUnPublishProccess() async {
    isLoadingCreateFaq.value = true;
    isSuccessCreateFaq.value = false;
    final result = await _apiService.postCreateFaq(
        questionUnPublishFaqController.text,
        answerUnPublishFaqController.text,
        valueDropdownUnPublishFaq.value == 'publish' ? true : false);
    if (result.code == 200) {
      isLoadingCreateFaq.value = false;
      isSuccessCreateFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingCreateFaq.value = false;
      isSuccessCreateFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> updateFaqPublishProccess() async {
    isLoadingUpdateFaq.value = true;
    isSuccessUpdateFaq.value = false;
    final result = await _apiService.postUpdateFaq(
        faqId.value,
        questionPublishFaqController.text,
        answerPublishFaqController.text,
        valueDropdownPublishFaq.value == 'publish' ? true : false);
    if (result.code == 200) {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> updateFaqUnPublishProccess() async {
    isLoadingUpdateFaq.value = true;
    isSuccessUpdateFaq.value = false;
    final result = await _apiService.postUpdateFaq(
        faqId.value,
        questionUnPublishFaqController.text,
        answerUnPublishFaqController.text,
        valueDropdownUnPublishFaq.value == 'publish' ? true : false);
    if (result.code == 200) {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> deleteFaqPublishProccess() async {
    isLoadingDeleteFaq.value = true;
    isSuccessDeleteFaq.value = false;
    final result = await _apiService.postDeleteFaq(faqId.value);
    if (result.code == 200) {
      isLoadingDeleteFaq.value = false;
      isSuccessDeleteFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingDeleteFaq.value = false;
      isSuccessDeleteFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> deleteFaqUnPublishProccess() async {
    isLoadingDeleteFaq.value = true;
    isSuccessDeleteFaq.value = false;
    final result = await _apiService.postDeleteFaq(faqId.value);
    if (result.code == 200) {
      isLoadingDeleteFaq.value = false;
      isSuccessDeleteFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingDeleteFaq.value = false;
      isSuccessDeleteFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }


  Future<bool> unPublishFaqProccess() async {
    isLoadingUpdateFaq.value = true;
    isSuccessUpdateFaq.value = false;
    final result = await _apiService.postUpdateFaq(
        faqId.value,
        questionPublishFaqController.text,
        answerPublishFaqController.text,
        false);
    if (result.code == 200) {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> publishFaqProccess() async {
    isLoadingUpdateFaq.value = true;
    isSuccessUpdateFaq.value = false;
    final result = await _apiService.postUpdateFaq(
        faqId.value,
        questionPublishFaqController.text,
        answerPublishFaqController.text,
        true);
    if (result.code == 200) {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = true;
      Fluttertoast.showToast(msg: result.message);
      return true;
    } else {
      isLoadingUpdateFaq.value = false;
      isSuccessUpdateFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      return false;
    }
  }

  Future<bool> detailFaqProccess() async {
    isLoadingDetailFaq.value = true;
    isSuccessDetailFaq.value = false;
    final result = await _apiService.getDetailFaq(faqId.value);
    if (result.code == 200) {
      isLoadingDetailFaq.value = false;
      isSuccessDetailFaq.value = true;
      dataDetailFaq = result.data ?? DataDetailFaq();
      return true;
    } else {
      isLoadingDetailFaq.value = false;
      isSuccessDetailFaq.value = false;
      Fluttertoast.showToast(msg: result.message);
      dataDetailFaq = DataDetailFaq();
      return false;
    }
  }
}

enum PublishStatus { PUBLISH, UNPUBLISH }
