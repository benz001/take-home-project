import 'package:flutter/material.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:take_home_project/helper/access_token_helper.dart';
import 'package:take_home_project/service/api_service.dart';
import 'package:take_home_project/ui/faq_list_page.dart';

class AuthenticationController extends GetxController {
  final APIService _apiService = APIService();
  final AccessTokenHelper _accessTokenHelper = AccessTokenHelper();
  final formKey = GlobalKey<FormBuilderState>();
  RxBool isLoadingLogin = false.obs;
  RxBool isSuccessLogin = false.obs;
  TextEditingController emaillTextEditingController =
      TextEditingController(text: '');
  TextEditingController passwordTextEditingController =
      TextEditingController(text: '');
  RxBool isShowPassword = false.obs;


  void loginProcess() async {
    isLoadingLogin.value = true;
    final result = await _apiService.postLogin(
        emaillTextEditingController.text, passwordTextEditingController.text);
    if (result.code == 200) {
      isLoadingLogin.value = false;
      isSuccessLogin.value = true;
      _accessTokenHelper.setAccessToken(result.data?.accessToken??'');
      Get.offAndToNamed(FaqListPage.routeName);
    } else {
      isLoadingLogin.value = false;
      isSuccessLogin.value = false;
      Fluttertoast.showToast(msg: result.message);
    }
  }

  void showPassword(bool status) {
    isShowPassword.value = status;
    update();
  }
}
