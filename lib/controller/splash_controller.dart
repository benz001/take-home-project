import 'package:get/get.dart';
import 'package:take_home_project/helper/access_token_helper.dart';
import 'package:take_home_project/ui/faq_list_page.dart';
import 'package:take_home_project/ui/login_page.dart';

class SplashController extends GetxController {
  final AccessTokenHelper _accessTokenHelper = AccessTokenHelper();

  @override
  void onInit() {
    checkUserAuthorization();
    super.onInit();
  }

  void checkUserAuthorization()async{
    final accessToken = await _accessTokenHelper.getAccessToken();
    if ((accessToken??'').isEmpty) {
      Get.toNamed(LoginPage.routeName);
    }else{
       Get.toNamed(FaqListPage.routeName);
    }
  }
  
}