import 'package:get/get.dart';
import 'package:take_home_project/controller/faq_list_controller.dart';


class FaqListBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => FaqListController());
  }
}