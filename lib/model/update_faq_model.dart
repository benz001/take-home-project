// To parse this JSON data, do
//
//     final UpdateFaqModel = UpdateFaqModelFromJson(jsonString);

import 'dart:convert';

UpdateFaqModel updateFaqModelFromJson(String str) => UpdateFaqModel.fromJson(json.decode(str));

String updateFaqModelToJson(UpdateFaqModel data) => json.encode(data.toJson());

class UpdateFaqModel {
    int code;
    String message;
    DataUpdateFaq? data;

    UpdateFaqModel({
        required this.code,
        required this.message,
        required this.data,
    });

    factory UpdateFaqModel.fromJson(Map<String, dynamic> json) => UpdateFaqModel(
        code: json["code"],
        message: json["message"],
        data: DataUpdateFaq.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": data?.toJson(),
    };
}

class DataUpdateFaq {
    String? pertanyaan;
    String? jawaban;
    bool? statusPublish;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    DataUpdateFaq({
         this.pertanyaan,
         this.jawaban,
         this.statusPublish,
         this.updatedAt,
         this.createdAt,
         this.id,
    });

    factory DataUpdateFaq.fromJson(Map<String, dynamic> json) => DataUpdateFaq(
        pertanyaan: json["pertanyaan"],
        jawaban: json["jawaban"],
        statusPublish: json["status_publish"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "pertanyaan": pertanyaan,
        "jawaban": jawaban,
        "status_publish": statusPublish,
        "updated_at": updatedAt?.toIso8601String(),
        "created_at": createdAt?.toIso8601String(),
        "id": id,
    };
}
