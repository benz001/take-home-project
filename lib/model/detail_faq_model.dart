// To parse this JSON data, do
//
//     final DetailFaqModel = DetailFaqModelFromJson(jsonString);

import 'dart:convert';

DetailFaqModel detailFaqModelFromJson(String str) => DetailFaqModel.fromJson(json.decode(str));

String detailFaqModelToJson(DetailFaqModel data) => json.encode(data.toJson());

class DetailFaqModel {
    int code;
    String message;
    DataDetailFaq? data;

    DetailFaqModel({
        required this.code,
        required this.message,
        required this.data,
    });

    factory DetailFaqModel.fromJson(Map<String, dynamic> json) => DetailFaqModel(
        code: json["code"],
        message: json["message"],
        data: DataDetailFaq.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": data?.toJson(),
    };
}

class DataDetailFaq {
    String? pertanyaan;
    String? jawaban;
    int? statusPublish;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    DataDetailFaq({
         this.pertanyaan,
         this.jawaban,
         this.statusPublish,
         this.updatedAt,
         this.createdAt,
         this.id,
    });

    factory DataDetailFaq.fromJson(Map<String, dynamic> json) => DataDetailFaq(
        pertanyaan: json["pertanyaan"],
        jawaban: json["jawaban"],
        statusPublish: json["status_publish"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "pertanyaan": pertanyaan,
        "jawaban": jawaban,
        "status_publish": statusPublish,
        "updated_at": updatedAt?.toIso8601String(),
        "created_at": createdAt?.toIso8601String(),
        "id": id,
    };
}
