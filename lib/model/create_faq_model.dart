// To parse this JSON data, do
//
//     final createFaqModel = createFaqModelFromJson(jsonString);

import 'dart:convert';

CreateFaqModel createFaqModelFromJson(String str) => CreateFaqModel.fromJson(json.decode(str));

String createFaqModelToJson(CreateFaqModel data) => json.encode(data.toJson());

class CreateFaqModel {
    int code;
    String message;
    DataCreateFaq? data;

    CreateFaqModel({
        required this.code,
        required this.message,
        required this.data,
    });

    factory CreateFaqModel.fromJson(Map<String, dynamic> json) => CreateFaqModel(
        code: json["code"],
        message: json["message"],
        data: DataCreateFaq.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": data?.toJson(),
    };
}

class DataCreateFaq {
    String? pertanyaan;
    String? jawaban;
    bool? statusPublish;
    DateTime? updatedAt;
    DateTime? createdAt;
    int? id;

    DataCreateFaq({
         this.pertanyaan,
         this.jawaban,
         this.statusPublish,
         this.updatedAt,
         this.createdAt,
         this.id,
    });

    factory DataCreateFaq.fromJson(Map<String, dynamic> json) => DataCreateFaq(
        pertanyaan: json["pertanyaan"],
        jawaban: json["jawaban"],
        statusPublish: json["status_publish"],
        updatedAt: DateTime.parse(json["updated_at"]),
        createdAt: DateTime.parse(json["created_at"]),
        id: json["id"],
    );

    Map<String, dynamic> toJson() => {
        "pertanyaan": pertanyaan,
        "jawaban": jawaban,
        "status_publish": statusPublish,
        "updated_at": updatedAt?.toIso8601String(),
        "created_at": createdAt?.toIso8601String(),
        "id": id,
    };
}
