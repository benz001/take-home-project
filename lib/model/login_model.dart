// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) => LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
    int code;
    String message;
    DataLogin? data;

    LoginModel({
        required this.code,
        required this.message,
         this.data,
    });

    factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        code: json["code"],
        message: json["message"],
        data: DataLogin.fromJson(json["data"]),
    );

    Map<String, dynamic> toJson() => {
        "code": code,
        "message": message,
        "data": data?.toJson(),
    };
}

class DataLogin {
    int? id;
    String? name;
    dynamic jenisUser;
    String? email;
    String? level;
    String? pathFoto;
    String? accessToken;
    String? tokenType;
    String? expiresIn;
    String? nik;

    DataLogin({
         this.id,
         this.name,
        this.jenisUser,
         this.email,
         this.level,
         this.pathFoto,
         this.accessToken,
         this.tokenType,
         this.expiresIn,
         this.nik,
    });

    factory DataLogin.fromJson(Map<String, dynamic> json) => DataLogin(
        id: json["id"],
        name: json["name"],
        jenisUser: json["jenis_user"],
        email: json["email"],
        level: json["level"],
        pathFoto: json["path_foto"],
        accessToken: json["access_token"],
        tokenType: json["token_type"],
        expiresIn: json["expires_in"],
        nik: json["nik"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "jenis_user": jenisUser,
        "email": email,
        "level": level,
        "path_foto": pathFoto,
        "access_token": accessToken,
        "token_type": tokenType,
        "expires_in": expiresIn,
        "nik": nik,
    };
}
