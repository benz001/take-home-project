
import 'package:get/get.dart';
import 'package:take_home_project/binding/authentication_binding.dart';
import 'package:take_home_project/binding/faq_list_binding.dart';
import 'package:take_home_project/binding/splash_binding.dart';
import 'package:take_home_project/ui/faq_list_page.dart';
import 'package:take_home_project/ui/login_page.dart';
import 'package:take_home_project/ui/splash_page.dart';

class Routes {
  static List<GetPage<dynamic>>? listPage() {
    return [
      GetPage(
          name: SplashPage.routeName,
          page: () => SplashPage(),
          binding: SplashBinding()),
       GetPage(
          name: LoginPage.routeName,
          page: () => LoginPage(),
          binding: AuthenticationBinding()),
      GetPage(
          name: FaqListPage.routeName,
          page: () => FaqListPage(),
          binding: FaqListBinding()),
    ];
  }
}